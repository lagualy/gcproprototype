(function() {
	angular
		.module('zoop.gc')
		.service('DataService', DataService);

	DataService.$inject = ['$rootScope', '$http', '$q'];

	function DataService($rootScope, $http, $q) {

		var self = this;

		self.request = request;

		function request(params) {
			var dest = 'app2/mock-data/' + params.requestKey;

			return $http({
				method: 'POST',
				url: dest
			});
		}

	}
})()
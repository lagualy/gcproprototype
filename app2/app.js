(function() {
	angular
		.module('zoop.gc', [
			'ngAnimate',
			'ui.router',
			'ui.bootstrap',
			'ngMap',
			'ngSanitize',
			'textAngular',
			'ui.select',
			'ngDragDrop',
			'daterangepicker',
			'datatables',
			'datatables.colreorder'
		]);
})()
(function () {

	angular
		.module('zoop.gc')
		.config(function($stateProvider, $urlRouterProvider) {

			$urlRouterProvider.otherwise('/ap');

			$stateProvider
				.state('ap', {
					url: '/ap',
					component: 'gcActionPlan'
				})
				.state('reports', {
					url: '/reports',
					component: 'gcReports'
				})

		})
})()

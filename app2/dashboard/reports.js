(function() {
	angular
		.module('zoop.gc')
		.component('gcReports', {
			templateUrl: 'app2/dashboard/view/reports.html',
			controller: 'ReportsController as vm'
		});

	angular
		.module('zoop.gc')
		.controller('ReportsController', ReportsController)
		.directive('reports', reports);

	ReportsController.$inject = ['$rootScope', '$scope', 'DataService'];

	function ReportsController($rootScope, $scope, DataService) {
		var vm = this;

		vm.$onInit = function() {
        	DataService.request({
        		requestKey: 'action-plans'
        	}).then(function(response) {
        		$scope.actionPlans = response.data.leadList;
        	});
		}

		$scope.tab = 1;

	    $scope.setTab = function(newTab){
	      $scope.tab = newTab;
	    };

	    $scope.isSet = function(tabNum){
	      return $scope.tab === tabNum;
	    };

	}

	function reports() {
		return {
			link: link
		};

		function link($scope, elem, attr) {

		    google.charts.load("visualization", "1", {'packages':['corechart']});
	      	google.charts.setOnLoadCallback(drawChart);

	      	var chart;

			function drawChart() {

	      		var data = new google.visualization.DataTable();
			    data.addColumn('string', 'Name');
			    data.addColumn('number', 'Open');
			    data.addColumn('number', 'Assigned');
			    data.addColumn('number', 'In Contact');
			    data.addColumn('number', 'Qouted');
			    data.addColumn('number', 'Closed');
			    // data.addColumn('number', 'Ticketed');
			    // data.addColumn('number', 'Lost');
			    data.addRows([
			        ['John', 	 2, 1, 0, 0, 0],
			        ['Maria',    2, 3, 4, 0, 0],
			        ['Vincent',  2, 2, 0, 0, 0],
			        ['Alex',   	 1, 2, 3, 4, 5],
			        ['Mark',     1, 0, 0, 0, 0]
			    ]);

			    var view = new google.visualization.DataView(data);
			    var seriesColors = ['#ebebeb','#cccccc','#b7b7b7','#a1a1a1','#898989'];

			    var options = {
		        	width: '100%',
	    			height: '350',
		        	backgroundColor: { fill: 'transparent'},
		        	isStacked: true,
		        	colors: seriesColors
			    };


			    var chart = new google.visualization.BarChart($('#userChart')[0]);
			    chart.draw(view, options);


			    // resolution

			    var data2 = new google.visualization.DataTable();
			    data2.addColumn('string', 'Name');
			   	data2.addColumn('number', 'Ticketed');
			    data2.addColumn('number', 'Lost');
			    data2.addRows([
			        ['John', 	 1000, -500],
			        ['Maria',    800, -1000],
			        ['Vincent',  1200, -200],
			        ['Alex',   	 1500, -0],
			        ['Mark',     1000, -400]
			    ]);

			    var view2 = new google.visualization.DataView(data2);
			    var seriesColors2 = ['#5cb85c','#d9534f'];

			    var options2 = {
		        	width: '100%',
	    			height: '350',
		        	backgroundColor: { fill: 'transparent'},
		        	isStacked: true,
		        	colors: seriesColors2
			    };

			    var formatter = new google.visualization.NumberFormat(
			    {prefix: '$', negativeColor: 'red', negativeParens: true});
				formatter.format(data2, 1);
				formatter.format(data2, 2);

			    var chart2 = new google.visualization.BarChart($('#resolutionChart')[0]);
			    chart2.draw(view2, options2);

			    $(document).ready(function(){
			    	$('.stages').change(function () {
			    		var cols = [0];
				    	var colors = [];  
				        $('.filters').find(':checkbox:checked').each(function () {
				            var value = parseInt($(this).attr('value'));
				            cols.push(value);
				            colors.push(seriesColors[value - 1]);
				        });
				        view.setColumns(cols);
				        
				        chart.draw(view, {
				        	colors: colors,
				        	isStacked: true
				        });
				    });

				    $('.resolutions').change(function () {
			    		var cols = [0];
				    	var colors = [];  
				        $('.filters').find(':checkbox:checked').each(function () {
				            var value = parseInt($(this).attr('value'));
				            cols.push(value);
				            colors.push(seriesColors2[value - 1]);
				        });
				        view2.setColumns(cols);
				        
				        chart2.draw(view2, {
				        	colors: colors,
				        	isStacked: true
				        });

				        $('.stages').prop('checked', false);
				        $('.countValue').show();
				    });

				    $('#resolutionChart').hide();

				    $('.resolutions').click(function(){
				    	$('.stages').prop('checked', false);
				    	$('#userChart').hide();
				    	$('#resolutionChart').show();
				    	$('.countValue').show();
				    });

				    $('.stages').click(function(){
				    	$('.resolutions').prop('checked', false);
				    	$('#resolutionChart').hide();
				    	$('#userChart').show();
				    });

				    $('.nav-pills>li>a').click(function(){
						var id = $(this).attr('id');

						switch(id){
							case 'byUser':
								data.removeRows(0,4);
								data2.removeRows(0,4);
								data.addRows([
							        ['John', 	 2, 1, 0, 0, 0],
							        ['Maria',    2, 3, 4, 0, 0],
							        ['Vincent',  2, 2, 0, 0, 0],
							        ['Alex',   	 1, 2, 3, 4, 5],
							        ['Mark',     1, 0, 0, 0, 0]
								]);
								data2.addRows([
							        ['John', 	 1000, -500],
							        ['Maria',    800, -1000],
							        ['Vincent',  1200, -200],
							        ['Alex',   	 1500, 0],
							        ['Mark',     1000, -400]
								]);


								formatter.format(data2, 1);
								formatter.format(data2, 2);

								chart.draw(view, options);
								chart2.draw(view2, options2);
								break;
							case 'byRegion':
								data.removeRows(0,4);
								data2.removeRows(0,4);
								data.addRows([
									['Central',  1, 2, 3, 4, 5],
			        				['East',     1, 0, 0, 0, 0],
			        				['West',     1, 0, 0, 0, 0]
								]);
								data2.addRows([
									['Central',  12000, -3000],
			        				['East',     32000, -2300],
			        				['West',     10000, -500]
								]);0


								formatter.format(data2, 1);
								formatter.format(data2, 2);

					      		chart.draw(view, options);
					      		chart2.draw(view2, options2);
								break;
							case 'byDistrict':
								data.removeRows(0,4);
								data2.removeRows(0,4);
								data.addRows([
									['District 1',   	 3, 4, 5, 3, 4],
			        				['District 2',     5, 4, 6, 2, 1],
			        				['District 3',     1, 0, 2, 0, 1]
								]);
								data2.addRows([
									['District 1',   	 3000, -500],
			        				['District 2',      2000, -3000],
			        				['District 3',      2300, 0]
								]);


								formatter.format(data2, 1);
								formatter.format(data2, 2);

					      		chart.draw(view, options);
					      		chart2.draw(view2, options2);
								break;
							case 'byStore':
								data.removeRows(0,4);
								data2.removeRows(0,4);
								data.addRows([
									['124 Bakersfield',  8, 11, 10, 22, 32],
			        				['442 Houston',     10, 8, 13, 12, 2],
			        				['District 3',     4, 0, 2, 15, 12]
								]);
								data2.addRows([
									['124 Bakersfield',  2300, -400],
			        				['442 Houston',     0, -300],
			        				['District 3',     1000, -200]
								]);


								formatter.format(data2, 1);
								formatter.format(data2, 2);

					      		chart.draw(view, options);
					      		chart2.draw(view2, options2);
								break;

						}
					});
			    });
		    }


		}
	}

})()
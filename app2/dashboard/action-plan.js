(function() {
	angular
		.module('zoop.gc')
		.component('gcActionPlan', {
			templateUrl: 'app2/dashboard/view/action-plan.html',
			controller: 'ActionPlanController as vm'
		});

	angular
		.module('zoop.gc')
		.controller('ActionPlanController', ActionPlanController)
		.controller('WithColReorderCtrl', WithColReorderCtrl);

	ActionPlanController.$inject = ['$rootScope', '$scope', '$filter', 'DataService'];
	WithColReorderCtrl.$inject =['DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder'];


	function ActionPlanController($rootScope, $scope, $filter, DataService) {
		var vm = this;

		vm.$onInit = function() {
        	DataService.request({
        		requestKey: 'action-plans'
        	}).then(function(response) {
        		$scope.actionPlans = response.data.leadList;

        		var filterByStatus = function(status) {
				  return function (x) {
				    return x.status == status;
				  };
				};

				$scope.open = $scope.actionPlans.filter(filterByStatus('open'));
				$scope.assigned = $scope.actionPlans.filter(filterByStatus('assigned'));
				$scope.inContact = $scope.actionPlans.filter(filterByStatus('inContact'));
				$scope.qouted = $scope.actionPlans.filter(filterByStatus('qouted'));
				$scope.closed = $scope.actionPlans.filter(filterByStatus('closed'));
        	});
		}

		var date = new Date();
		$scope.currDate = $filter('date')(new Date(), 'MM / d / yyyy');
        $scope.currTime = $filter('date')(new Date(), 'HH:mm:ss');

		$scope.emptyHistory = {
			"content": "",
			"postDate": $scope.currDate,
			"postTime": $scope.currTime,
			"by": ""
		}

		$scope.activeActionPlan = {
			"drag": true,
			"id": "",
			"title": "",
			"status": "",
			"salesRep": "",
			"phone": "",
			"email": "",
			"company": "",
			"desc": "",
			"assoc": "",
			"store": "",
			"salesID": "",
			"source": "",
			"dateCreated": "",
			"ellapsed": "1",
			"revenue": "",
			"history": [],
			"newHistory": angular.copy($scope.emptyHistory)
		};

		$scope.emptyActionPlan = {
			"drag": true,
			"id": "",
			"title": "",
			"status": "open",
			"salesRep": "",
			"phone": "",
			"email": "",
			"company": "",
			"desc": "",
			"assoc": "",
			"store": "",
			"salesID": "",
			"source": "",
			"dateCreated": "",
			"ellapsed": "1",
			"revenue": "",
			"history": [],
			"newHistory": angular.copy($scope.emptyHistory)
		};

		$scope.hist = {
			add:function(history, hist){
				$scope.activeActionPlan.newHistory = angular.copy($scope.emptyHistory);
				var newHistory = angular.copy(hist);
				history.push(newHistory);
			}
		};

		$scope.actionPlan = {
			add:function(action){
				var newAction = angular.copy(action);

				if(action.id == ""){					
					$scope.maxId = Math.max.apply(Math,$scope.actionPlans.map(function(action){return action.id;}));
					action.id = $scope.maxId + 1;
					$scope.actionPlans.push(action);
					$scope.open.push(action);
					$scope.activeActionPlan = angular.copy($scope.emptyActionPlan);

				}else{
					$scope.showForm = false;
				}

				angular.element('ul li').removeClass('active');
				$scope.showForm = false;
				console.log($scope.actionPlans);
			}
		};

		$scope.action = {};
		$scope.action.inputs = [];

		$scope.history = [];
		$scope.newHistory = [];

	    $scope.createActionPlan = function(){
	    	$scope.showForm = true;
	    	$scope.activeActionPlan = angular.copy($scope.emptyActionPlan);
	    }

	    $scope.editActionPlan = function(id){
	    	for(var i in $scope.actionPlans){
	    		if($scope.actionPlans[i].id == id){
	    			$scope.activeActionPlan = $scope.actionPlans[i];
	    			console.log($scope.actionPlans[i]);
	    		}
	    	}
	    	$scope.showForm = true;
	    }

	    $scope.updateLead = function(item){
	    	
	    }

	    $scope.cancel = function(){
	    	$scope.showForm = false;
	    }

	    $scope.stage = function(event){
	    	angular.element('ul.lead-stage-buttons li').removeClass('active');
	    	$(event.target).parent().addClass('active');
	    }

	    $scope.closeLead = function(event){
	    	$('#closeLead').modal();
	    }
	}

	function WithColReorderCtrl(DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder) {
	    var vm = this;
	    vm.dtOptions = DTOptionsBuilder.newOptions()
	        .withPaginationType('full_numbers')
	        .withColReorder()
	        .withColReorderCallback(function() {
	            console.log('Columns order has been changed with: ' + this.fnOrder());
	        });
	    vm.dtColumnDefs = [
			DTColumnBuilder.newColumn('leadID').withTitle('No move me!'),
	        DTColumnBuilder.newColumn('title').withTitle('Try to move me!'),
	        DTColumnBuilder.newColumn('salesRep').withTitle('You cannot move me! *evil laugh*'),
	        DTColumnBuilder.newColumn('phone').withTitle('No move me!'),
	        DTColumnBuilder.newColumn('email').withTitle('Try to move me!'),
	        DTColumnBuilder.newColumn('company').withTitle('You cannot move me! *evil laugh*'),
	        DTColumnBuilder.newColumn('store').withTitle('No move me!'),
	        DTColumnBuilder.newColumn('associate').withTitle('Try to move me!'),
	        DTColumnBuilder.newColumn('salesID').withTitle('You cannot move me! *evil laugh*'),
	        DTColumnBuilder.newColumn('dateCreated').withTitle('You cannot move me! *evil laugh*'),
	        DTColumnBuilder.newColumn('assigned').withTitle('No move me!'),
	        DTColumnBuilder.newColumn('stage').withTitle('Try to move me!'),
	        DTColumnBuilder.newColumn('actions').withTitle('You cannot move me! *evil laugh*'),
	    ];
	}
})()
(function() {
    angular
        .module('zoop.gc')
        .directive('gcEditActionPlan', gcEditActionPlan);

        gcEditActionPlan.$inject = ['$window'];

        function gcEditActionPlan($window) {
            return {
                scope: {
                    step: '=',
                    index: '=',
                    toggled: '='
                },
                templateUrl: 'app2/dashboard/view/edit-action-plan.html',
                link: function(scope, elem, attrs) {

                    
                }
            }
        }
})()

(function() {

    $('.cx-step-popup').hide();

    $('.cx-step').on("click", function() {
        $('.cx-step-popup').hide();
        $(this).find('.cx-step-popup').toggle();
    })

    $('.cx-step-popup-close').on("click", function(e) {
        $(this).parent().hide();
        e.stopPropagation();
    })

    $('.cx-step-popup').off("click", function() {
        $('.cx-step-popup').hide();
    })
})()
